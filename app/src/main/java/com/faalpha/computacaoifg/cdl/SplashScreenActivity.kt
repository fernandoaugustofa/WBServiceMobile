package com.faalpha.computacaoifg.cdl

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.net.ConnectivityManager
import android.widget.Toast
import android.content.DialogInterface
import android.support.v7.app.AlertDialog


class SplashScreenActivity : AppCompatActivity() {
    private var alerta: AlertDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        val handle = Handler()
        handle.postDelayed({ mostrarLogin() }, 2000)
    }



    private fun mostrarLogin() {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        intent = Intent(this@SplashScreenActivity, Login::class.java)
        if (networkInfo != null && networkInfo.isConnected) {
            intent = Intent(this@SplashScreenActivity, Login::class.java)
            startActivity(intent)
            finish()
        } else {
            exemplo_simples()
        }


    }
    private fun exemplo_simples() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Sem conexãp")
        builder.setMessage("Verifique a conexão e tente novamente!")
        //builder.setPositiveButton("Entendido!", { arg0, arg1 -> Toast.makeText(this, "positivo=$arg1", Toast.LENGTH_SHORT).show() })
        alerta = builder.create()
        alerta!!.show()
    }

}