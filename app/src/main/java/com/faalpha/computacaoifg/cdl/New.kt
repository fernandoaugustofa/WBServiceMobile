package com.faalpha.computacaoifg.cdl

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.new_agenda.*
import org.json.JSONObject
import java.io.DataOutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.net.UnknownHostException
import android.widget.ArrayAdapter
import org.jetbrains.anko.custom.async
import android.app.DatePickerDialog
import java.util.*
import java.text.SimpleDateFormat
import android.app.TimePickerDialog
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import java.text.Normalizer



class New : AppCompatActivity() {
    var Funcionario:String = ""
    var Url:String = ""
    var Empresas = ArrayList<String>()
    private var alerta: AlertDialog? = null
    var myCalendar = Calendar.getInstance()
    var date: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        updateLabel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_agenda)
        val extras = intent.extras
        Url = extras.getString("url")
        Funcionario = extras.getString("Funcionario")
        async {
            update()
        }
    }


    fun calendar(V: View){
        DatePickerDialog(this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show()
    }

    @SuppressLint("SetTextI18n")
    fun time_new(V: View){
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mcurrentTime.get(Calendar.MINUTE)

        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute -> hora_new.setText(selectedHour.toString() + ":" + selectedMinute) }, hour, minute, true)
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }


    private fun updateLabel() {
        val myFormat = "dd/MM/yyyy"
        val sdf = SimpleDateFormat(myFormat, Locale("pt", "BR"))
        data_new.setText(sdf.format(myCalendar.time))
    }


    fun update(){
        try {
            val url =Url+"/Empresas"
            val json = URL(url).readText()
            val gson = Gson()
            var list_empresas: List<filiado> = gson.fromJson(json, object : TypeToken<List<filiado>>() {}.type)
            list_empresas.forEach {
                Empresas.add(it.full_name.toString())
            }

            runOnUiThread {
                val adapter = ArrayAdapter<String>(this, android.R.layout.select_dialog_item, Empresas)
                Empresa_new.setAdapter(adapter)
            }

        }catch (e: UnknownHostException) {
            runOnUiThread {
                Toast.makeText(this@New, "Desculpe, Não foi possivel carregar os filiados", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun send(V: View) {
        if (TextUtils.isEmpty(Empresa_new.text)){
            Empresa_new.setError("Nome da empresa necessário!")
            return
        }
        if (TextUtils.isEmpty(Endereco_new.text)){
            Endereco_new.setError("Endereço necessário!")
            return
        }
        if (TextUtils.isEmpty(data_new.text)){
            data_new.setError("Data necessário!")
            return
        }
        if (TextUtils.isEmpty(hora_new.text)){
            hora_new.setError("Hora necessário!")
            return
        }
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Salvar")
        builder.setMessage("Deseja salvar este novo agendamento?")
        builder.setPositiveButton("Sim!") { _, _ -> sendPost() }
        builder.setNegativeButton("Não!") { _, _ -> alerta?.dismiss(); }
        alerta = builder.create()
        alerta!!.show()
    }

    fun removeAcentos(str: String): String {
        var str = str
        str = Normalizer.normalize(str, Normalizer.Form.NFD)
        str = str.replace("[^\\p{ASCII}]".toRegex(), "")
        return str

    }


    @SuppressLint("SimpleDateFormat")
    fun sendPost() {
        val thread = Thread(Runnable {
            try {
                val url = URL(Url+"/Inserir")
                val conn = url.openConnection() as HttpURLConnection
                conn.requestMethod = "PUT"
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8")
                conn.setRequestProperty("Accept", "application/json")
                conn.doOutput = true
                conn.doInput = true
                val data = JSONObject()
                data.put("Empresa", removeAcentos(Empresa_new.text.toString()))
                var date = data_new.text.toString()
                var spf = SimpleDateFormat("dd/MM/yyyy")
                val newDate = spf.parse(date)
                spf = SimpleDateFormat("yyyy-MM-dd")
                date = spf.format(newDate)
                data.put("Data", date)
                data.put("Hora", hora_new.text)
                data.put("Endereco", removeAcentos(Endereco_new.text.toString()))
                data.put("Fone_fixo", fone_fixo_new.text)
                data.put("Celular", celular_new.text)
                data.put("Representante", removeAcentos(Representante_new.text.toString()))
                data.put("Funcionario", Funcionario)
                Log.i("JSON", data.toString())
                val os = DataOutputStream(conn.outputStream)
                os.writeBytes(data.toString())
                os.flush()
                os.close()
                Log.i("MSG", conn.responseMessage)
                conn.disconnect()
                val intent = Intent(this@New, Menu::class.java)
                intent.putExtra("Funcionario", Funcionario)
                intent.putExtra("url", Url)
                startActivity(intent)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })

        thread.start()
    }

}