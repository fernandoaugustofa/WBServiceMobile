package com.faalpha.computacaoifg.cdl
/**
 * Created by Fernando Augusto on 11/06/2018.
 */
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import org.jetbrains.anko.custom.async
import java.net.URL
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.net.UnknownHostException
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.new_agenda.*
import org.jetbrains.anko.progressDialog
import org.json.JSONObject
import java.text.Normalizer
import java.text.SimpleDateFormat
import java.util.*


class MainActivity_search : AppCompatActivity(){
    var isFABOpen:Boolean = false
    var name:String? = null
    var date:String?= null
    var done:String?= null
    var Url:String = ""
    private var adapter:ToDoAdapter?= null
    var database:MyDatabaseOpenHelper?=null
    var todoCursor:Cursor?=null
    var sql:String?= "Select * from agenda"
    var Funcionario:String = ""
    var myCalendar = Calendar.getInstance()
    @SuppressLint("SetTextI18n")
    var date_picker1: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        Date01.setText(dayOfMonth.toString()+"/"+(monthOfYear+1)+"/"+year)
    }
    @SuppressLint("SetTextI18n")
    var date_picker2: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        Date02.setText(dayOfMonth.toString()+"/"+(monthOfYear+1)+"/"+year)
    }

    fun update(){
        try {
            database = MyDatabaseOpenHelper.getInstance(applicationContext)
            database?.use {
                delete("agenda", "", null)
            }
            var json = ""
            val url =Url+"/"+Funcionario
            json = URL(url).readText()
            val gson = Gson()
            var agenda: List<Horas> = gson.fromJson(json, object : TypeToken<List<Horas>>() {}.type)
            agenda.forEach {
                insert(it.ID, it.Empresa, it.Data, it.Status, it.Hora, it.Endereco,it.Fone_fixo,it.Celular,it.Representante)
            }
            database?.close()
            database = MyDatabaseOpenHelper.getInstance(applicationContext)
            todoCursor = database!!.writableDatabase.rawQuery(sql, null)
            adapter = ToDoAdapter(this@MainActivity_search, todoCursor!!)
            runOnUiThread {
                lvItems_s.adapter=adapter
                adapter?.notifyDataSetChanged()
                Toast.makeText(this@MainActivity_search, "Atualizado", Toast.LENGTH_SHORT).show()
            }
        }catch (e: UnknownHostException) {
            runOnUiThread {
                    Toast.makeText(this@MainActivity_search, "Falha, Desculpe", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun reflesh(){
        database = MyDatabaseOpenHelper.getInstance(applicationContext)
        todoCursor= database!!.writableDatabase.rawQuery(sql,null)
        adapter= ToDoAdapter(this,todoCursor!!)
        lvItems_s.adapter=adapter
    }

    fun insert(id:Int, Empresa: String, Data: String, Status: Int, hora:String,endereco:String,fone:String,celular:String,representante:String){
        database = MyDatabaseOpenHelper.getInstance(applicationContext)
        database?.use {
            val values = ContentValues()
            values.put("_id", id)
            values.put("empresa", Empresa)
            values.put("date", Data)
            values.put("status", Status)
            values.put("hora", hora)
            values.put("endereco", endereco)
            values.put("fone_fixo", fone)
            values.put("celular", celular)
            values.put("representante", representante)
            insert("agenda",null,values)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        val extras = intent.extras
        Funcionario = extras.getString("Funcionario")
        Url = extras.getString("url")
        sql = extras.getString("sql")
        async {
            update()
            runOnUiThread {
                reflesh()
            }
        }
    }

    fun calendar1(V: View){
        DatePickerDialog(this, date_picker1, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show()
    }
    fun calendar2(V: View){
        DatePickerDialog(this, date_picker2, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show()
    }

    @SuppressLint("SimpleDateFormat")
    fun pesquisa(v: View){
        var date = Date01.text.toString()
        var date2 = Date02.text.toString()
        var spf = SimpleDateFormat("dd/MM/yyyy")
        val newDate = spf.parse(date)
        val newDate2 = spf.parse(date2)
        spf = SimpleDateFormat("yyyy-MM-dd")
        date = spf.format(newDate)
        date2 = spf.format(newDate2)
        sql = "select * from agenda where date >= '$date' and date <= '$date2'"
        reflesh()
    }

}
