package com.faalpha.computacaoifg.cdl
/**
 * Created by Fernando Augusto on 11/06/2018.
 */
import android.content.Intent
import android.icu.text.Normalizer
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.view.*
import org.json.JSONObject
import java.io.DataOutputStream
import java.net.HttpURLConnection
import java.net.URL




class SendFeed : AppCompatActivity() {
    var Funcionario:String = ""
    var Url:String = ""
    private var alerta: AlertDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view)
        val extras = intent.extras
        val id = extras.getString("id")
        Url = extras.getString("url")
        Funcionario = extras.getString("Funcionario")
        floatingActionButton.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Salvar")
            builder.setMessage("Deseja salvar este feedback?")
            builder.setPositiveButton("Sim!") { _, _ -> sendPost(id.toInt()) }
            builder.setNegativeButton("Não!") { _, _ -> alerta?.dismiss(); }
            alerta = builder.create()
            alerta!!.show()
        }
    }

    fun stripAccents(s: String): String {
        var s = s
        s = java.text.Normalizer.normalize(s, java.text.Normalizer.Form.NFD)
        s = s.replace("[\\p{InCombiningDiacriticalMarks}]".toRegex(), "")
        return s
    }

    fun sendPost(id:Int) {
        val thread = Thread(Runnable {
            try {
                val url = URL(Url)
                val conn = url.openConnection() as HttpURLConnection
                conn.requestMethod = "PUT"
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8")
                conn.setRequestProperty("Accept", "application/json")
                conn.doOutput = true
                conn.doInput = true
                val data = JSONObject()
                data.put("ID", id)
                data.put("Realizado", switch1.isChecked.toString())
                data.put("check1", radioButton1.isChecked.toString())
                data.put("check2", radioButton2.isChecked.toString())
                data.put("check3", radioButton3.isChecked.toString())
                data.put("check4", radioButton4.isChecked.toString())
                data.put("check5", radioButton5.isChecked.toString())
                data.put("check6", radioButton6.isChecked.toString())
                data.put("check7", radioButton7.isChecked.toString())
                data.put("obs", stripAccents(editText.text.toString()))
                Log.i("JSON", data.toString())
                val os = DataOutputStream(conn.outputStream)
                os.writeBytes(data.toString())
                os.flush()
                os.close()
                Log.i("MSG", conn.responseMessage)
                conn.disconnect()
                val intent = Intent(this@SendFeed, Menu::class.java)
                intent.putExtra("Funcionario", Funcionario)
                intent.putExtra("url", Url)
                startActivity(intent)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })

        thread.start()
    }

    fun realizada(V: View){
        if (switch1.isChecked){
            radioButton1.text = "NF-e"
            radioButton2.text = "CDL Celular"
            radioButton3.text = "Central de Cobranças"
            radioButton4.text = "SPC Brasil"
            radioButton5.text = "CDL Saúde"
            radioButton6.text = "Escola de Negocios"
            radioButton7.visibility = View.VISIBLE
        }else {
            radioButton1.text = "Desmarcada pelo cliente"
            radioButton2.text = "Cliente não estava"
            radioButton3.text = "Desmarcada pelo execultor"
            radioButton4.text = "Cliente nao atendeu"
            radioButton5.text = "Executor não compareceu"
            radioButton6.text = "Desmarcada pela gerencia"
            radioButton7.visibility = View.GONE
        }
    }

}