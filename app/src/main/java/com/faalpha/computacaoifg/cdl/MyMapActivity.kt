package com.faalpha.computacaoifg.cdl
/**
 * Created by Fernando Augusto on 11/06/2018.
 */
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.CameraUpdateFactory


class MyMapActivity :FragmentActivity(),OnMapReadyCallback {
    var currentLatitude:Double = 0.0
    var currentLongitude:Double = 0.0
    var Empresa:String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.mapf)
        val extras = intent.extras
        currentLatitude = extras.getDouble("currentLatitude")
        currentLongitude = extras.getDouble("currentLongitude")
        Empresa = extras.getString("Empresa")
        val mapFragment = fragmentManager.findFragmentById(R.id.map) as MapFragment
        mapFragment.getMapAsync(this)

    }

    override fun onMapReady(mMap: GoogleMap?) {
        val location = LatLng(currentLatitude,currentLongitude)
        mMap?.addMarker(MarkerOptions()
                .position(location)
                .title(Empresa))

        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 70f))
    }


}
