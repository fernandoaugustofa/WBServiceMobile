package com.faalpha.computacaoifg.cdl

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import com.lmntrx.android.library.livin.missme.ProgressDialog
import kotlinx.android.synthetic.main.login.*
import org.jetbrains.anko.toast
import android.provider.SyncStateContract.Helpers.update
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.view.*
import org.jetbrains.anko.custom.async
import org.json.JSONObject
import java.io.DataOutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import kotlin.experimental.and


class Login :Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

    }

    fun login(V:View) {

        btn_login.setEnabled(false)

        val progressDialog = ProgressDialog(this@Login)
        progressDialog.setIndeterminate(true)
        progressDialog.setMessage("Autenticando...")
        progressDialog.show()
        var pass = hashString("SHA-512", input_password.text.toString())


        async {
            var user = User.text.toString()
            var url =  "${getString(R.string.Url_Service)}/IniciarSessao/${(User.text.toString())}/${(pass)}"
            val retorno = URL(url).readText()
            runOnUiThread {
                btn_login.setEnabled(true)
            }
            if(retorno.equals("True")){
                val intent = Intent(this@Login, Menu::class.java)
                intent.putExtra("Funcionario", user)
                intent.putExtra("url", "${getString(R.string.Url_Service)}" )

                startActivity(intent)
            }else{
                runOnUiThread {
                    Toast.makeText(this@Login, "Usuario ou senhas Incorretas", Toast.LENGTH_SHORT).show()
                }
            }
            progressDialog.dismiss()

        }
        async {
            var user = User.text.toString()
            var url = "${getString(R.string.Url_Service2)}/IniciarSessao/${(User.text.toString())}/${(pass)}"
            val retorno = URL(url).readText()
            runOnUiThread {
                btn_login.setEnabled(true)
            }
            if(retorno.equals("True")){
                val intent = Intent(this@Login, Menu::class.java)
                intent.putExtra("Funcionario", user)
                intent.putExtra("url", "${getString(R.string.Url_Service2)}" )
                startActivity(intent)
            }else{
                runOnUiThread {
                    Toast.makeText(this@Login, "Usuario ou senhas Incorretas", Toast.LENGTH_SHORT).show()
                }
            }
            progressDialog.dismiss()

        }

    }

    private fun hashString(type: String, input: String): String {
        val HEX_CHARS = "0123456789ABCDEF"
        val bytes = MessageDigest
                .getInstance(type)
                .digest(input.toByteArray())
        val result = StringBuilder(bytes.size * 2)

        bytes.forEach {
            val i = it.toInt()
            result.append(HEX_CHARS[i shr 4 and 0x0f])
            result.append(HEX_CHARS[i and 0x0f])
        }

        return result.toString()
    }

    fun Insert() {
        val thread = Thread(Runnable {
            try {
                var pass = hashString("SHA-512", input_password.text.toString())
                val url = URL(getString(R.string.Url_Service)+"/IniciarSessao")
                val conn = url.openConnection() as HttpURLConnection
                conn.requestMethod = "PUT"
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8")
                conn.setRequestProperty("Accept", "application/json")
                conn.doOutput = true
                conn.doInput = true
                val data = JSONObject()
                data.put("Name", User.text.toString())
                data.put("Pass", pass)
                Log.i("JSON", data.toString())
                val os = DataOutputStream(conn.outputStream)
                os.writeBytes(data.toString())
                os.flush()
                os.close()
                Log.i("MSG", conn.responseMessage)
                conn.disconnect()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })

        thread.start()
    }

}