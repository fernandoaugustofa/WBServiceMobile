package com.faalpha.computacaoifg.cdl
/**
 * Created by Fernando Augusto on 11/06/2018.
 */
import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.custom.async
import java.net.URL
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.net.UnknownHostException
import android.widget.AdapterView.OnItemClickListener
import android.widget.TextView
import android.widget.Toast
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import org.jetbrains.anko.progressDialog
import java.text.Normalizer


class MainActivity : AppCompatActivity(){
    var name:String? = null
    var date:String?= null
    var done:String?= null
    var Url:String = ""
    private var adapter:ToDoAdapter?= null
    var database:MyDatabaseOpenHelper?=null
    var todoCursor:Cursor?=null
    var sql:String?= "Select * from agenda"
    var Funcionario:String = ""

    fun update(){
        try {
            database = MyDatabaseOpenHelper.getInstance(applicationContext)
            database?.use {
                delete("agenda", "", null)
            }
            val url =Url+"/"+Funcionario
            val json = URL(url).readText()
            val gson = Gson()
            var agenda: List<Horas> = gson.fromJson(json, object : TypeToken<List<Horas>>() {}.type)
            agenda.forEach {
                insert(it.ID, it.Empresa, it.Data, it.Status, it.Hora, it.Endereco,it.Fone_fixo,it.Celular,it.Representante)
            }
            database?.close()
            database = MyDatabaseOpenHelper.getInstance(applicationContext)
            todoCursor = database!!.writableDatabase.rawQuery(sql, null)
            adapter = ToDoAdapter(this@MainActivity, todoCursor!!)
            runOnUiThread {
                lvItems.adapter=adapter
                adapter?.notifyDataSetChanged()
                Toast.makeText(this@MainActivity, "Atualizado", Toast.LENGTH_SHORT).show()
            }
        }catch (e: UnknownHostException) {
            runOnUiThread {
                    Toast.makeText(this@MainActivity, "Falha, Desculpe", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun reflesh(){
        database = MyDatabaseOpenHelper.getInstance(applicationContext)
        todoCursor= database!!.writableDatabase.rawQuery(sql,null)
        adapter= ToDoAdapter(this,todoCursor!!)
        lvItems.adapter=adapter
    }

    fun insert(id:Int, Empresa: String, Data: String, Status: Int, hora:String,endereco:String,fone:String,celular:String,representante:String){
        database = MyDatabaseOpenHelper.getInstance(applicationContext)
        database?.use {
            val values = ContentValues()
            values.put("_id", id)
            values.put("empresa", Empresa)
            values.put("date", Data)
            values.put("status", Status)
            values.put("hora", hora)
            values.put("endereco", endereco)
            values.put("fone_fixo", fone)
            values.put("celular", celular)
            values.put("representante", representante)
            insert("agenda",null,values)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val extras = intent.extras
        Funcionario = extras.getString("Funcionario")
        sql = extras.getString("sql")
        Url = extras.getString("url")

        this.floatingActionButton7.setOnClickListener {
            Toast.makeText(this@MainActivity, "Atualizando...", Toast.LENGTH_SHORT).show()
            async {
                floatingActionButton7.animate().rotationBy(-360f)
                update()

            }

        }

        lvItems.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            val Empresa_view = (view.findViewById<TextView>(R.id.Empresa))
            val Endereco_view = (view.findViewById<TextView>(R.id.endereco))
            val Done_view = (view.findViewById<TextView>(R.id.Done))
            val idlist = lvItems.getAdapter().getItemId(position).toInt()
            if (Done_view.text == "Pendente") {

                    val intent = Intent(this@MainActivity, Viagem::class.java)
                    val fone = todoCursor?.getString(todoCursor?.getColumnIndex("fone_fixo")!!)
                    val celular = todoCursor?.getString(todoCursor?.getColumnIndex("celular")!!)
                    val representante = todoCursor?.getString(todoCursor?.getColumnIndex("representante")!!)
                    intent.putExtra("_id", idlist.toString())
                    intent.putExtra("Funcionario", Funcionario)
                    intent.putExtra("empresa", Empresa_view.text)
                    intent.putExtra("endereco", Endereco_view.text)
                    intent.putExtra("fone", fone)
                    intent.putExtra("celular", celular)
                    intent.putExtra("representante", representante)
                    intent.putExtra("url", Url)
                    startActivity(intent)
            }
            if (Done_view.text == "Aguardando Feedback") {
                val intent = Intent(this@MainActivity, SendFeed::class.java)
                intent.putExtra("id", idlist.toString())
                intent.putExtra("url", Url)
                intent.putExtra("Funcionario", Funcionario)
                startActivity(intent)
            }
        }
        async {
            update()
            runOnUiThread {
                reflesh()
            }
        }
    }
}
