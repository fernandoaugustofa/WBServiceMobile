package com.faalpha.computacaoifg.cdl
/**
 * Created by Fernando Augusto on 11/06/2018.
 */
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CursorAdapter
import android.widget.RelativeLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.card.*
import org.jetbrains.anko.progressDialog
import org.json.JSONObject
import java.net.URL
import java.text.Normalizer

class ToDoAdapter(context: Context, cursor:Cursor):
        CursorAdapter(context,cursor,0){
    override fun newView(p0: Context?, p1: Cursor?, p2: ViewGroup?): View {
      return LayoutInflater.from(p0).inflate(R.layout.card,p2,false)
    }

    override fun bindView(view: View?, context: Context?, cursor: Cursor?) {
        var S = ""
        val name =  cursor?.getString(cursor.getColumnIndexOrThrow("empresa"))
        val endereco = cursor?.getString(cursor.getColumnIndexOrThrow("endereco"))
        val status = cursor?.getInt(cursor.getColumnIndexOrThrow("status"))
        val hora = cursor?.getString(cursor.getColumnIndexOrThrow("hora"))
        (view?.findViewById<TextView>(R.id.Empresa))?.text=name
        (view?.findViewById<TextView>(R.id.endereco))?.text= endereco
        (view?.findViewById<TextView>(R.id.Hora))?.text= hora

        if (status == 1) {
            S = "Pendente"
            (view?.findViewById<TextView>(R.id.Done))?.setTextColor(Color.parseColor("#eeeeee"))
            //(view?.findViewById<RelativeLayout>(R.id.card))?.setBackgroundResource(R.color.Pendente)
        }
        if (status == 2) {
            S = "Concluido"
            (view?.findViewById<TextView>(R.id.Done))?.setTextColor(Color.parseColor("#a5d6a7"))
            //(view?.findViewById<RelativeLayout>(R.id.card))?.setBackgroundResource(R.color.Concluido)
        }
        if (status == 3) {
            S = "Falha"
            (view?.findViewById<TextView>(R.id.Done))?.setTextColor(Color.parseColor("#ef9a9a"))
            //(view?.findViewById<RelativeLayout>(R.id.card))?.setBackgroundResource(R.color.Falha)
        }
        if (status == 4) {
            S = "Aguardando Feedback"
            (view?.findViewById<TextView>(R.id.Done))?.setTextColor(Color.parseColor("#FFD27F"))
           // (view?.findViewById<RelativeLayout>(R.id.card))?.setBackgroundResource(R.color.Pendente)
        }
        (view?.findViewById<TextView>(R.id.Done))?.text=  S

    }
}