package com.faalpha.computacaoifg.cdl

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity

class Finish : AppCompatActivity() {
    var user:String = ""
    var Url:String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.finish)
        val extras = intent.extras
        user = extras.getString("Funcionario")
        Url = extras.getString("url")
        val handle = Handler()
        handle.postDelayed({ mostrarLogin() }, 2000)
    }

    private fun mostrarLogin() {
        val intent = Intent(this@Finish,
                Menu::class.java)
        intent.putExtra("Funcionario", user)
        intent.putExtra("url", Url)
        startActivity(intent)
        finish()
    }

}