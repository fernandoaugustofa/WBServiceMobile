package com.faalpha.computacaoifg.cdl
/**
 * Created by Fernando Augusto on 11/06/2018.
 */
import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.View
import kotlinx.android.synthetic.main.time.*
import java.net.URL
import android.util.Log
import android.widget.Chronometer
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.jetbrains.anko.progressDialog
import org.json.JSONObject
import java.io.DataOutputStream
import java.net.HttpURLConnection
import java.text.Normalizer


class Viagem :FragmentActivity(), LocationProvider.LocationCallback {
    var currentLatitude:Double = 0.0
    var currentLongitude:Double = 0.0
    var Empresa:String = ""
    var Fone:String = ""
    var Celular:String = ""
    var Representante:String = ""
    var id:Int = 1
    var Funcionario:String = ""
    var Endereco:String = ""
    var Url:String = ""
    private var mLocationProvider: LocationProvider? = null


    fun update(){
            val url =Url+"/feed"+id
            val json = URL(url).readText()
            val gson = Gson()
            var Feed: List<feed> = gson.fromJson(json, object : TypeToken<List<Horas>>() {}.type)
            radioButton11.setChecked(Feed[0].check1)
            radioButton21.setChecked(Feed[0].check1)
            radioButton31.setChecked(Feed[0].check1)
            radioButton41.setChecked(Feed[0].check1)
            radioButton51.setChecked(Feed[0].check1)
            radioButton61.setChecked(Feed[0].check1)
            radioButton71.setChecked(Feed[0].check1)
    }


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        mLocationProvider = LocationProvider(this, this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.time)
        val extras = intent.extras
        Empresa = extras.getString("empresa")
        Funcionario = extras.getString("Funcionario")
        Fone = extras.getString("fone")
        Celular = extras.getString("celular")
        Representante = extras.getString("representante")
        Url = extras.getString("url")
        time_empresa.text = Empresa
        time_fone.text = "Fone Fixo: $Fone"
        textView8.text ="Celular: $Celular"
        Endereco =  extras.getString("endereco")
        time_endereco.text = extras.getString("endereco")
        time_representante.text = "Representante: $Representante"
        val id2 = extras.getString("_id")
        id = extras.getString("_id").toInt()
        //update()
    }

    fun Mapa(v:View){
        val progressDialog = progressDialog("Please wait")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val thread = Thread(Runnable {
            var aa = Endereco
            aa = Normalizer.normalize(aa, Normalizer.Form.NFD)
            aa = aa.replace("[\\p{InCombiningDiacriticalMarks}]", "")
            aa = aa.replace("\\s".toRegex(), "+")
            aa = aa.replace("[^A-Za-z0-9+ ]".toRegex(), "")
            val json = URL("https://maps.googleapis.com/maps/api/geocode/json?address=$aa&key=AIzaSyD5KJfS0n8U7-rrP60hmJxDBXPQDKu1vQo").readText()
            progressDialog.progress = 50
            val reader = JSONObject(json)
            progressDialog.progress = 70
            val sys = reader.getJSONArray("results").getJSONObject(0)
            progressDialog.progress = 80
            val sys1 = sys.getJSONObject("geometry").getJSONObject("location")
            progressDialog.progress = 90
            val currentLatitude = sys1.getDouble("lat")
            val currentLongitude = sys1.getDouble("lng")
            progressDialog.progress = 100
            val intent = Intent(this@Viagem, MyMapActivity::class.java)
            intent.putExtra("currentLatitude", currentLatitude)
            intent.putExtra("currentLongitude", currentLongitude)
            intent.putExtra("Empresa", Empresa)
            progressDialog.dismiss()
            startActivity(intent)
        })
        thread.start()
    }


    @SuppressLint("SetTextI18n")
    fun inioiar_e_encerrar_viagem(V: View){
        if (button2.text == "Iniciar Deslocamento"){
            val thread = Thread(Runnable {
                val url = URL(Url+"/Start/${this.id}")
                val conn = url.openConnection() as HttpURLConnection
                conn.requestMethod = "PUT"
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8")
                conn.setRequestProperty("Accept", "application/json")
                conn.doOutput = true
                conn.doInput= true
                val data = JSONObject()
                data.put("latitude", currentLatitude)
                data.put("longitude", currentLongitude)
                val os = DataOutputStream(conn.outputStream)
                os.writeBytes(data.toString())
                os.flush()
                os.close()
                Log.i("MSG", conn.responseMessage)
                conn.disconnect()

            })
            thread.start()
            val simpleChronometer = findViewById<Chronometer>(R.id.simpleChronometer)
            simpleChronometer.start()
            button2.text = "Finalizar Deslocamento"
        }else {
            val intent = Intent(this@Viagem, Menu::class.java)
            val thread = Thread(Runnable {
                val url = URL(Url+"/Stop/${this.id}")
                val conn = url.openConnection() as HttpURLConnection
                conn.requestMethod = "PUT"
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8")
                conn.setRequestProperty("Accept", "application/json")
                conn.doOutput = true
                conn.doInput= true
                val data = JSONObject()
                data.put("latitude", currentLatitude)
                data.put("longitude", currentLongitude)
                val os = DataOutputStream(conn.outputStream)
                os.writeBytes(data.toString())
                os.flush()
                os.close()
                Log.i("MSG", conn.responseMessage)
                conn.disconnect()

            })
            thread.start()
            simpleChronometer.stop()
            intent.putExtra("Funcionario", Funcionario)
            intent.putExtra("url", Url)
            startActivity(intent)
        }

    }



    override fun onResume() {
        super.onResume()
        mLocationProvider!!.connect()
    }
    override fun onPause() {
        super.onPause()
        mLocationProvider!!.disconnect()
    }
    override fun handleNewLocation(location: Location) {
        currentLatitude = location.latitude
        currentLongitude = location.longitude
    }

}
