package com.faalpha.computacaoifg.cdl
/**
 * Created by Fernando Augusto on 11/06/2018.
 */
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "MyDatabase") {
    companion object {
        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            instance = instance ?: MyDatabaseOpenHelper(ctx.applicationContext)
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        // Here you create tables
        db.createTable("agenda", true,
                "_id" to INTEGER + PRIMARY_KEY ,
                "empresa" to INTEGER ,
                "date" to TEXT,
                "hora" to TEXT,
                "endereco" to TEXT,
                "fone_fixo" to TEXT,
                "celular" to TEXT,
                "representante" to TEXT,
                "status" to INTEGER)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable("agenda")

        // Here you can upgrade tables, as usual
    }
}