package com.faalpha.computacaoifg.cdl

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.start.*
import java.text.SimpleDateFormat
import java.util.*

class Menu  : AppCompatActivity(){
    var sql:String? = null
    var user:String = ""
    var Url:String = ""
    private val RECORD_REQUEST_CODE = 101
    private val RECORD_REQUEST_CODE2 = 100
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.start)
        val extras = intent.extras
        user = extras.getString("Funcionario")
        funcionario_menu.text = "Olá, "+ user
        Url = extras.getString("url")
        setupPermissions()
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val permission2 = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        }
        if (permission2 != PackageManager.PERMISSION_GRANTED) {
            makeRequest2()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                RECORD_REQUEST_CODE)
    }

    private fun makeRequest2() {
        ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION),
                RECORD_REQUEST_CODE2)
    }

    @SuppressLint("SimpleDateFormat")
    fun visitahoje(v: View){
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val currentDateandTime = sdf.format(Date())
        sql= "Select * from agenda where status = 1 and date = '$currentDateandTime'"
        val intent = Intent(this@Menu, MainActivity::class.java)
        intent.putExtra("Funcionario", user)
        intent.putExtra("sql", sql)
        intent.putExtra("url", Url)
        startActivity(intent)
    }

    fun fechamento(v: View){
        sql= "Select * from agenda where status = 4"
        val intent = Intent(this@Menu, MainActivity::class.java)
        intent.putExtra("Funcionario", user)
        intent.putExtra("sql", sql)
        intent.putExtra("url", Url)
        startActivity(intent)
    }

    fun agendatotal(v: View){
        sql= "Select * from agenda"
        val intent = Intent(this@Menu, MainActivity_search::class.java)
        intent.putExtra("Funcionario", user)
        intent.putExtra("sql", sql)
        intent.putExtra("url", Url)
        startActivity(intent)
    }

    fun executadas(v: View){
        sql= "Select * from agenda where status <> 1"
        val intent = Intent(this@Menu, MainActivity::class.java)
        intent.putExtra("Funcionario", user)
        intent.putExtra("sql", sql)
        intent.putExtra("url", Url)
        startActivity(intent)
    }
    fun Novo(v: View){
        val intent = Intent(this@Menu, New::class.java)
        intent.putExtra("Funcionario", user)
        intent.putExtra("url", Url)
        startActivity(intent)
    }
    fun sair(v:View){
        finishAffinity()
        System.exit(0)
    }
}